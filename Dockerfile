FROM composer:latest

USER root
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv && \
    apk add --update git zip freetype libpng libjpeg-turbo \
    freetype-dev libpng-dev libjpeg-turbo-dev autoconf g++ make icu-dev \
    libxml2-dev php-soap

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php
RUN docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg

RUN docker-php-ext-install pdo pdo_mysql gd intl soap

RUN pecl install xdebug && docker-php-ext-enable xdebug

